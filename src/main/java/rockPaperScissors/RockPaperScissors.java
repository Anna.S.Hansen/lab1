package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
       


        // Input from computer 
        Random rand = new Random();
       

    
        System.out.println("Let's play round " + roundCounter);
        while (true) {
            // Input from user 
            System.out.println("Your choice (Rock/Paper/Scissors)? ");
            String humanChoice = sc.nextLine();
            String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            if (rpsChoices.contains(humanChoice)) {
                // Wins
                System.out.print("Human chose " + humanChoice + ", computer chose " + computerChoice + ". ");
                if (humanChoice.equals(computerChoice)) {
                    System.out.println("It's a tie");
                } else if (HumanWins(humanChoice, computerChoice)) {
                    System.out.println("Human wins!");
                    humanScore ++;
                } else {
                    System.out.println("Computer wins!");
                    computerScore ++;
                }
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                System.out.println("Do you wish to continue playing? (y/n)? ");
                String continueAnswer = sc.nextLine();
                if (continueAnswer.equals("y")){
                    roundCounter ++;
                    System.out.println("Let's play round " + roundCounter);
                } else {
                    System.out.println("Bye bye :)");
                    break;
                }
            } else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                continue;   
            }
        }
    }
    public boolean HumanWins(String humanChoice, String computerChoice){
        if (humanChoice.equals("rock")) {
            return computerChoice.equals("scissors");
        } else if (humanChoice.equals("paper")) {
            return computerChoice.equals("rock");
        } else {
            return computerChoice.equals("paper");
        }
    }    
   

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
